import { createClient } from 'contentful'

const SPACE_ID = 'newiup8yel3w'
const ACCESS_TOKEN = '77d84702af747a174f88b4f193b320a42c4b7948c8d27a83587338038aa8f745'

const client = createClient({
  space: SPACE_ID,
  accessToken: ACCESS_TOKEN,
})

export default async function getData () {
  const contentTypes = await client.getContentTypes()
  return contentTypes.items
}
