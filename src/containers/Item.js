import React from 'react'
import { withRouteData, Link } from 'react-static'
//

export default withRouteData(({ item }) => (
  <div>
    <Link to="/items/">{'<'} Back</Link>
    <br />
    <h3>{item.title}</h3>
    <p>{item.created}</p>
  </div>
))
