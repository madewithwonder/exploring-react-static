
import React from 'react'
import { withRouteData, Link } from 'react-static'
//

export default withRouteData(({ items }) => (
  <div>
    <h1>Here's items from contenful.</h1>
    <br />
    All content types:
    <ul>
      {items.map(item => (
        <li key={item.nid}>
          <Link to={`/items/item/${item.nid}/`}>{item.title}</Link>
        </li>
      ))}
    </ul>
  </div>
))
