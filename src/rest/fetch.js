
const axios = require('axios')

export async function getArticles () {

  const response = await axios.get('http://18.196.251.29/api/article');
  console.log(response.data)
  return response.data

}

export async function getArticle (article) {

  const response = await axios.get(`http://18.196.251.29/api/article/${article.nid}`);
  console.log(response.data)
  return response.data[0]

}

