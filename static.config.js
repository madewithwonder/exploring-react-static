import { getArticles, getArticle } from './src/rest/fetch'

import { _ } from 'lodash'

export default {
  getSiteData: () => ({
    title: 'React Static Contentful',
  }),
  getRoutes: async () => {
    
    // get all articles
    const articles = await getArticles()

    // get all article details
    const articleDetails = await Promise.all(articles.map(getArticle))

    const articleDetailsOrganized = {};

    _.each(articleDetails, a => {
      articleDetailsOrganized[a.nid] = a
    })

    return [
      {
        path: '/',
        component: 'src/containers/Home',
      },
      {
        path: '/about',
        component: 'src/containers/About',
      },
      {
        path: '/items',
        component: 'src/containers/Items',
        getData: () => ({
          items: articles,
        }),
        children: articles.map(article => ({
          path: `/item/${article.nid}`,
          component: 'src/containers/Item',
          getData: () => ({
            item: articleDetailsOrganized[article.nid],
          }),
        })),
      },
      {
        is404: true,
        component: 'src/containers/404',
      },
    ]
  },
}
